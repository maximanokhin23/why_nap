let googleTranslate = require("../fabrics/googleTranslateFabric"),
    client = require('../fabrics/googleSpeechFabric');

module.exports = {
    
    getEnglishTranslate: (words, lang) => new Promise((resolve, reject) => {
        googleTranslate.translate(words, lang, 'en',
            function (err, translations) {
                if (err) {
                    reject(err);
                }
                resolve(translations.map((wordData) => wordData.translatedText));
            }
        );
    }),
    getWordsFromVideoData: (binaryData) => new Promise((resolve, reject) => {
        client.recognize(binaryData)
            .then(async data => resolve(data))
            .catch(err => reject(err));
    })
    
    
};
