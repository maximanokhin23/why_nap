'use strict';
const ffmpeg = require('fluent-ffmpeg');
const mime = require('mime');
const fs = require('fs');
module.exports = {};
module.exports.extractAudio = (filePathIn, filePathOut) => new Promise((resolve, reject) => {
    if (!filePathIn || !filePathOut) {
        throw new Error('You must specify a path for both input and output files.');
    }
    if (!fs.existsSync(filePathIn)) {
        throw new Error('Input file must exist.');
    }
    
    try {
        ffmpeg(filePathIn)
            .audioChannels(0)
            .outputOptions([
                '-f s16le',
                '-acodec pcm_s16le',
                '-vn',
                '-ac 1',
                '-ar 16k',
                '-map_metadata -1'
            ])
            .save(filePathOut)
            .on('end', () => resolve(filePathOut));
    } catch (e) {
        reject(e);
    }
});

module.exports.speechToWordTime = (speechResponse) => {
    const wortTime = speechResponse[0].results[0].alternatives[0].words.map((wordData) => {
            return {
                time: wordData.startTime.seconds,
                word: wordData.word
            }
        }),
        words = wortTime.map((wordData) => wordData.word);
    return {
        wortTime,
        words
    }
}
