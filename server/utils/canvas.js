const {Image} = require('canvas'),
    extend = require('util')._extend;

module.exports = {};
module.exports.Particle = Particle;
let gravity = 0.05;

function Particle(canvas, picture, time) {
    let w = canvas.width,
        h = canvas.height;
    this.x = w / 2;
    this.y = h / 2;
    this.vx = Math.random() * -4 + 2;
    this.vy = Math.random() * 5;
    this.ax = 0;
    this.time = time;
    this.ay = gravity;
    this.picture = picture;
};