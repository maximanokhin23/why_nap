const path = require('path');
module.exports = (req, file, cb) => {
    
    const filetypes = /mov|mp4|quicktime|avi/,
        mimetype = filetypes.test(file.mimetype),
        extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    
    if (mimetype && extname) {
        return cb(null, true);
    }
    
    cb(new Error("Error: File upload only supports the following filetypes - " + filetypes));
};