module.exports = {
    enableWordTimeOffsets: true,
    encoding: 'LINEAR16',
    sampleRateHertz: 16000,
    languageCode: 'ru',
};