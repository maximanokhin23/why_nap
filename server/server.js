const
    //system
    express = require('express'),
    cors = require('cors'),
    app = express(),
    ffmpeg = require('fluent-ffmpeg'),
    fs = require('fs'),
    exec = require('await-exec'),
    crypto = require('crypto'),
    
    // canvas
    fontkit = require('fontkit'),
    font = fontkit.openSync('emojione-mac.ttc').fonts[0],
    {createCanvas, loadImage, Image} = require('canvas'),
    GIFEncoder = require('gifencoder'),
    canvasHelper = require('./utils/canvas'),
    
    // custom
    helper = require('./utils/helper'),
    translate = require('moji-translate'),
    multer = require('./fabrics/multerFabric'),
    googleRepository = require('./repositories/googleRepository'),
    googleSpeechConfig = require("./configs/googleSpeech");

app.use(cors());

app.post('/', (req, res) => {
    
    // Загрузка файла
    multer('file')(req, res, async (err) => {
        
        let empjisOnTime = null;
        
        // Если файл не смогли загрузить то ошибка
        if (err) {
            return res.status(500).send('Something broke!');
        }
        
        try {
            
            // работа с ffmpeg - берем звуковую дорожку
            const filename = req.file.filename.split('.')[0],
                filepath = '/var/www/photolab/upload/tmp',
                audioPath = filepath + '/' + filename + ".raw";
            await helper.extractAudio(req.file.path, audioPath);
            
            // кодируем звук для распознования текста
            const file = fs.readFileSync(audioPath),
                audioBytes = file.toString('base64'),
                audio = {content: audioBytes},
                request = {audio: audio, config: googleSpeechConfig};
            
            // получаем слова в определенное врем
            let speechResponse = await googleRepository.getWordsFromVideoData(request),
                englishWord = null;
            
            speechResponse = helper.speechToWordTime(speechResponse);
            
            // получаем их анлгийский перевод для составления emoji
            if (googleSpeechConfig.languageCode == 'en') {
                englishWord = speechResponse.words;
            } else {
                englishWord = await googleRepository.getEnglishTranslate(speechResponse.words, googleSpeechConfig.languageCode);
            }
            
            // формируем массив эмоджи по времени
            empjisOnTime = englishWord.map(
                (word, index) => {
                    let textEmoji = translate.translate(word, true);
                    return {
                        // rонвертируем эмодзи в виртуальную картинку
                        picture: textEmoji &&
                        font.layout(textEmoji) &&
                        font.layout(textEmoji).glyphs &&
                        font.layout(textEmoji).glyphs[0] &&
                        textEmoji != '' ? font.layout(textEmoji).glyphs[0].getImageForSize(64).data : null,
                        second: speechResponse.wortTime[index].time
                    }
                }
            );
            
            
            let encoder = new GIFEncoder(500, 800),
                canvas = createCanvas(500, 800),
                ctx = canvas.getContext('2d'),
                gifName = filepath + '/' + filename + '.gif';
            console.log(gifName);
            encoder.createReadStream().pipe(fs.createWriteStream(gifName));
            encoder.start();
            encoder.setTransparent(0xFF00FF);
            encoder.setRepeat(0);
            encoder.setDelay(20),
                canvas.getContext('2d');
            
            let particles = [];
            // соханяем все картинки
            for (let i = 0; i < empjisOnTime.length; i++) {
                let emojiObj = empjisOnTime[i];
                if (emojiObj && emojiObj.picture) {
                    let img = new Image;
                    img.src = emojiObj.picture;
                    let particle = new canvasHelper.Particle(canvas, img, emojiObj.second);
                    if (i % 2 == 0) {
                        particle.x += 25;
                    } else {
                        particle.x -= 25;
                    }
                    particles.push(particle);
                }
            }
            
            ffmpeg.ffprobe(req.file.path, async function (err, metadata) {
                
                if (err) {
                    console.log(err);
                    return res.status(500).send('Something broke!');
                }
                
                let changeParticlePosition = (particle) => {
                    particle.vx += particle.ax;
                    particle.vy += particle.ay;
                    particle.vx *= 0.99;
                    particle.vy *= 0.99;
                    particle.x += particle.vx;
                    particle.y += particle.vy;
                    
                    ctx.drawImage(particle.picture, particle.x, particle.y);
                    encoder.addFrame(ctx);
                }
                
                let oneSecond = 21;
                let cadrs = 0;
                console.log((metadata.format.duration * oneSecond) + ' all cadrs count');
                while (cadrs < metadata.format.duration * oneSecond) {
                    // while (cadrs < 30) {
                    
                    console.log(cadrs);
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctx.fillStyle = 'rgba(0,0,0,0)';
                    ctx.fillRect(0, 0, canvas.width, canvas.height);
                    
                    particles.map((partice) => {
                        // тут проеряем что время выводить эмоджу
                        if (partice.time <= cadrs / oneSecond) {
                            changeParticlePosition(partice);
                        }
                    });
                    encoder.addFrame(ctx);
                    cadrs++;
                    
                }
                
                encoder.finish();
                const { stdout, stderr } = await exec('ffmpeg -y -i ' + req.file.path + ' -i ' + gifName + ' -filter_complex "[0:v][1:v] overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/1.5:enable=\'between(t,0,20)\'" -pix_fmt yuv420p -c:a copy ' + req.file.path + 'out.MOV');
                console.log('stdout:', stdout);
    
                res.send(req.file.filename + 'out.MOV');
            });
            
        } catch (err) {
            console.log(err);
            return res.status(500).send('Something broke!');
        }
        
    })
});

app.listen(3000, () => console.log('server running on port 3000'));