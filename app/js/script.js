$(document).ready(function () {
    
    
    /**
     * Ждем пока файл не удет выбран
     */
    $("#sign-form button").on('click', function () {
        
        $("#loader").show();
        var formData = new FormData();
        formData.append('file', $('#sign-form #file-for-sign')[0].files[0]);
        
        if ($('#sign-form #file-for-sign')[0].files.length == 0) {
            alert('yout not loading file');
            $("#loader").hide();
            return;
        }
        
        $.ajaxSetup({
            beforeSend: function (jqXHR, settings) {
                if (settings.dataType === 'binary') {
                    settings.xhr().responseType = 'arraybuffer';
                    settings.processData = false;
                }
            }
        })
        
        $.ajax({
            url: 'http://195.161.62.176:3000/',
            type: 'POST',
            data: formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success: function (data) {
                window.location.href = data;
                $("#loader").hide();
            },
            error: function () {
                alert('Ooops... sorry error :)')
                $("#loader").hide();
            }
        });
        
    });
    
    
});


